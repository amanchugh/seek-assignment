package com.seek.extractos

import com.seek.CarTimeRecord
import org.scalatest.FunSpec

class TotalCarsExtractorTest extends FunSpec {

  describe("TotalCarsExtractorTest") {

    it("should extract total number of cars seen ") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4),
        CarTimeRecord("2016-12-01T07:00:00", 23),
        CarTimeRecord("2016-12-01T08:00:00", 40),
        CarTimeRecord("2016-12-01T09:00:00", 10),
        CarTimeRecord("2016-12-02T05:00:00", 31),
        CarTimeRecord("2016-12-02T06:30:00", 41),
        CarTimeRecord("2016-12-02T07:00:00", 2),
        CarTimeRecord("2016-12-02T08:00:00", 4),
        CarTimeRecord("2016-12-02T09:00:00", 13),
        CarTimeRecord("2016-12-03T08:00:00", 4),
        CarTimeRecord("2016-12-04T09:00:00", 13),
        CarTimeRecord("2016-12-04T08:00:00", 4),
        CarTimeRecord("2016-12-04T09:00:00", 13),
        CarTimeRecord("2016-12-04T08:00:00", 4),
        CarTimeRecord("2016-12-04T09:00:00", 13))
      val result = TotalCarsExtractor.extract(sampleData)
      assert(!result.isEmpty)
      assert(result.equals("222"))
    }

    it("should extract total number of cars seen limited input ") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4))
      val result = TotalCarsExtractor.extract(sampleData)
      assert(!result.isEmpty)
      assert(result.equals("7"))
    }
    it("total cars should return empty string when no input provided") {

      val result = TotalCarsExtractor.extract(List.empty[CarTimeRecord])
      assert(result.eq(""))
    }

  }
}
