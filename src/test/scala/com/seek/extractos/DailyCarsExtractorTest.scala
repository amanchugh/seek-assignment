package com.seek.extractos

import com.seek.CarTimeRecord
import org.scalatest.FunSpec

class DailyCarsExtractorTest extends FunSpec {
  val sampleParamMap = Map("recordSeparator" -> " ", "topXHalfHours" -> "3")
  describe("DailyCarsExtractorTest") {

    it("extract daily cars total") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4),
        CarTimeRecord("2016-12-01T07:00:00", 23),
        CarTimeRecord("2016-12-01T08:00:00", 40),
        CarTimeRecord("2016-12-01T09:00:00", 10),
        CarTimeRecord("2016-12-02T05:00:00", 31),
        CarTimeRecord("2016-12-02T06:30:00", 41),
        CarTimeRecord("2016-12-02T07:00:00", 2),
        CarTimeRecord("2016-12-02T08:00:00", 4),
        CarTimeRecord("2016-12-02T09:00:00", 13),
        CarTimeRecord("2016-12-03T08:00:00", 4),
        CarTimeRecord("2016-12-03T09:00:00", 13))
      val result = DailyCarsExtractor.extract(sampleData, Some(sampleParamMap))
      assert(!result.isEmpty)
      assert(result.contains("2016-12-01 80"))
      assert(result.contains("2016-12-02 91"))
      assert(result.contains("2016-12-03 17"))
    }

    it("extract daily cars where input is only of one day") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4),
        CarTimeRecord("2016-12-01T07:00:00", 23),
        CarTimeRecord("2016-12-01T08:00:00", 40),
        CarTimeRecord("2016-12-01T09:00:00", 10),
        CarTimeRecord("2016-12-01T11:00:00", 3),
        CarTimeRecord("2016-12-01T12:30:00", 4),
        CarTimeRecord("2016-12-01T17:00:00", 23)
      )
      val result = DailyCarsExtractor.extract(sampleData, Some(sampleParamMap))
      assert(!result.isEmpty)
      assert(result.equals("2016-12-01 110"))
    }

    it("extract daily cars where input has no record") {
      val sampleData = List()
      val result = DailyCarsExtractor.extract(sampleData, Some(sampleParamMap))
      assert(result.isEmpty)
    }
  }
}
