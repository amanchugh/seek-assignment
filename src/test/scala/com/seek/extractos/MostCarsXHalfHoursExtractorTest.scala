package com.seek.extractos

import com.seek.CarTimeRecord
import org.scalatest.FunSpec

class MostCarsXHalfHoursExtractorTest extends FunSpec {

  val sampleParamMap = Map("recordSeparator" -> " ", "topXHalfHours" -> "3")
  describe("DailyCarsExtractorTest") {

    it("extract Most cars for X hours") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4),
        CarTimeRecord("2016-12-01T07:00:00", 23),
        CarTimeRecord("2016-12-01T08:00:00", 40),
        CarTimeRecord("2016-12-01T09:00:00", 10),
        CarTimeRecord("2016-12-02T05:00:00", 31),
        CarTimeRecord("2016-12-02T06:30:00", 41),
        CarTimeRecord("2016-12-02T07:00:00", 2),
        CarTimeRecord("2016-12-02T08:00:00", 4),
        CarTimeRecord("2016-12-02T09:00:00", 13),
        CarTimeRecord("2016-12-03T08:00:00", 4),
        CarTimeRecord("2016-12-03T09:00:00", 13))
      val result = MostCarsXHalfHoursExtractor.extract(sampleData, Some(sampleParamMap))
      assert(!result.isEmpty)
      assert(result.contains("2016-12-02T06:30:00 41"))
      assert(result.contains("2016-12-01T08:00:00 40"))
      assert(result.contains("2016-12-02T05:00:00 31"))
    }

    it("extract records with most cats input < 3 ") {
      val sampleData = List(
        CarTimeRecord("2016-12-01T05:00:00", 3),
        CarTimeRecord("2016-12-01T06:30:00", 4)
      )
      val result = MostCarsXHalfHoursExtractor.extract(sampleData, Some(sampleParamMap))
      assert(!result.isEmpty)
      assert(result == "2016-12-01T06:30:00 4\n2016-12-01T05:00:00 3")
    }

    it("extract top 3 with no records should return empty response") {
      val sampleData = List()
      val result = MostCarsXHalfHoursExtractor.extract(sampleData, Some(sampleParamMap))
      assert(result.isEmpty)
    }
  }
}
