package com.seek

import org.mockito.Mockito._
import org.scalatest.FunSpec
import org.scalatest.mockito.MockitoSugar

class ProcessVehicleInfoTest extends FunSpec with MockitoSugar {

  val sampleInput =
    """2016-12-01T05:00:00 5
      |2016-12-01T05:30:00 12
      |2016-12-01T06:00:00 14
      |2016-12-01T06:30:00 15
      |2016-12-01T07:00:00 25
      |2016-12-01T07:30:00 46
      |2016-12-01T08:00:00 42
      |2016-12-01T15:00:00 9
      |2016-12-01T15:30:00 11
      |2016-12-01T23:30:00 0
      |2016-12-05T09:30:00 18
      |2016-12-05T10:30:00 15
      |2016-12-05T11:30:00 7
      |2016-12-05T12:30:00 6
      |2016-12-05T13:30:00 9
      |2016-12-05T14:30:00 11
      |2016-12-05T15:30:00 15
      |2016-12-08T18:00:00 33
      |2016-12-08T19:00:00 28
      |2016-12-08T20:00:00 25
      |2016-12-08T21:00:00 21
      |2016-12-08T22:00:00 16
      |2016-12-08T23:00:00 11
      |2016-12-09T00:00:00 4
      |"""

  describe("ProcessVehicleInfoTest") {
    val processVehicle = new ProcessVehicleInfo()

    val processVehicleMock = mock[ProcessVehicleInfo]
    when(processVehicleMock.readFile("TestFile.txt")).
      thenReturn(Some(sampleInput.split("\n").toList))

    it("should calculateTransformations") {
      val sampleData = Some(sampleInput.split("\n").toList)
      val transformedIds = processVehicle.transformData(sampleData)
      val result = processVehicle.calculateTransformations(transformedIds)
      assert(result.size > 0)
      assert(result(0).contains(398))
      assert(result(1).contains("2016-12-09 4"))
      assert(result(3).contains("2016-12-01 179"))
      assert(result(7).contains("2016-12-08T18:00:00 33"))
    }

    it("transformData should  convert string to case classes") {
      val sampleData = List("2016-12-01T05:00:00 5", "2016-12-01T05:30:00 12")
      val result = processVehicle.transformData(Some(sampleData))
      assert(!result.isEmpty)
      assert(result.size == 2)
      assert(result(0).count == 5)
      assert(result(1).count == 12)
    }
  }

}
