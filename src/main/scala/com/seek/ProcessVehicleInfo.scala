package com.seek

import com.seek.extractos.{DailyCarsExtractor, MostCarsXHalfHoursExtractor, TotalCarsExtractor}
import com.typesafe.scalalogging.LazyLogging

import scala.io.Source
import scala.util.Try

/**
  * Created by aman on 5/20/2018 AD .
  */
class ProcessVehicleInfo extends LazyLogging {
  val RECORD_SPLITTER = " "

  def processFile(path: String): String = {
    val dataOption = readFile(path)
    val transformedRecords = transformData(dataOption)
    val resultList = calculateTransformations(transformedRecords)
    resultList.mkString("\n")
  }

  def calculateTransformations(recods: List[CarTimeRecord]): List[String] = {
    val exTotal = TotalCarsExtractor.extract(recods)
    val exDaily = DailyCarsExtractor.extract(recods)
    val exMostCars = MostCarsXHalfHoursExtractor.extract(recods)
    List(exTotal, exDaily, exMostCars)
  }

  def readFile(path: String): Option[List[String]] = {
    Try {
      Source.fromFile(path).getLines().toList
    }.toOption
  }

  def transformData(dataOption: Option[List[String]]): List[CarTimeRecord] = {
    dataOption match {
      case Some(data: List[String]) => {
        data.map { record =>
          val recArr = record.split(RECORD_SPLITTER)
          CarTimeRecord(recArr(0), recArr(1).toLong)
        }
      }
      case None => {
        logger.error("The input File has no data")
        List.empty
      }
    }
  }


}
