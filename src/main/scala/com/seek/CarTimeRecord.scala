package com.seek

/**
  * Created by aman on 5/20/2018 AD .
  */
case class CarTimeRecord(halfHourlyTime: String, count: Long) {
  override def toString = s"$halfHourlyTime $count"
}
