package com.seek

import com.typesafe.scalalogging.LazyLogging

/**
  * Created by aman on 5/20/2018 AD .
  */
object AIPSMain extends App with LazyLogging {
  val processVehicalInfo = new ProcessVehicleInfo
  if (args.length == 0)
    logger.error("please enter a file path from where to read the data")

  else {
    println(processVehicalInfo.processFile(args(0)))
  }


}
