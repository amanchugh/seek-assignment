package com.seek.extractos

import com.seek.CarTimeRecord

/**
  * Created by aman on 5/20/2018 AD .
  */
object TotalCarsExtractor extends InformationExtractor {

  override def extract(records: List[CarTimeRecord], otherParam: Option[Map[String, String]]) = {
    if (records.size == 0) ""
    else records.map(_.count).foldLeft(0l)(_ + _).toString()
  }

}
