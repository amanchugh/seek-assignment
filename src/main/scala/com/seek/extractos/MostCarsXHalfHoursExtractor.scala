package com.seek.extractos

import com.seek.CarTimeRecord

/**
  * Created by aman on 5/20/2018 AD .
  */
object MostCarsXHalfHoursExtractor extends InformationExtractor {

  override def extract(records: List[CarTimeRecord], otherParam: Option[Map[String, String]]) = {
    val topXHalfHours = otherParam.map(_.get("topXHalfHours")).flatten.getOrElse("3").toInt
    records.sortBy(_.count).reverse.take(topXHalfHours).mkString("\n")
  }
}
