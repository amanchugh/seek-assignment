package com.seek.extractos

import com.seek.CarTimeRecord

/**
  * Created by aman on 5/20/2018 AD .
  */
trait InformationExtractor {
  def extract(records: List[CarTimeRecord], otherParams: Option[Map[String, String]] = None): String
}
