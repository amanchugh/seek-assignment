package com.seek.extractos

import com.seek.CarTimeRecord

/**
  * Created by aman on 5/20/2018 AD .
  */
object DailyCarsExtractor extends InformationExtractor {

  override def extract(records: List[CarTimeRecord], otherParam: Option[Map[String, String]]): String = {
    val result = records
      .groupBy(_.halfHourlyTime.split("T")(0))
      .mapValues(_.map(_.count).sum)

    result.map { record => s"${record._1} ${record._2}"
    }.mkString("\n")
  }
}
