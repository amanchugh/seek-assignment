Seek Assignment

This project is build using scala and sbt.

Main Tasks 

1. Count the total number of cars in the file
2. Total Cars per day.
3. 3 Max half hours that registered most cars.

How to run 
1. Using Command Line 
sbt "run-main com.seek.AIPSMain /Users/achugh/learn/seek-assignment/sample.txt"
sbt "run-main {mainclass} {args}"

2. Using Intellij/ Eclipse
  1. Import Project
  2. Run configuration 
      Main Class: com.seek.AIPSMain
      Program Args /Users/achugh/learn/seek-assignment/sample.txt
  
  Test coverage  ~ 87% for Methods and 90% for Lines
